<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ImageController extends Controller
{

    /**Upload user photo
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function imageUpload(Request $request)
    {
        $this->validate($request, ['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg']);

        $image = Input::file('image');
        $filename = time() . '.' . $image->getClientOriginalExtension();


        //create image_path
        $path = public_path('images/' . $filename);
        //create image
        Image::make($image->getRealPath())->resize($request->width, $request->height)->save($path);

        //write log data(/storage/logs/laravel.log)
        Log::info(date("Y-m-d H:i:s") . 'Add image' . ' file_name:' . $filename . ' width:' . $request->width . ' height:' . $request->height . ' image size:' . $this->bytesToHuman($request->image->getClientSize()));
        return response()->json(['status' => 'ok', 'image' => $filename]);
    }

    /**Convert size to reposne see
     * @param $bytes
     * @return string
     */
    private function bytesToHuman($bytes)
    {
        $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];

        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }

        return round($bytes, 2) . ' ' . $units[$i];
    }

}
