<!DOCTYPE html>

<html>

<head>

    <title>Morele image upload</title>

    <link rel="stylesheet" href="http://getbootstrap.com/dist/css/bootstrap.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <script
            src="https://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
            crossorigin="anonymous"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div id="upload_box" style="margin-top: 20%;">
                <h1 style="text-align: center;">Image uploader</h1>
                <form id="upload_form" method="post" enctype="multipart/form-data">


                    <div class="form-group">
                        <input type="file" class="form-control" name="image" id="image" required/>
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control" name="height" id="height" placeholder="Enter height"
                               required/>
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control" name="width" id="width" placeholder="Enter width"
                               required/>
                    </div>

                    <button type="submit" class="btn btn-success">Upload</button>
                </form>
            </div>

            <div id="image_box">

            </div>


        </div>
        <div class="col-md-3"></div>

    </div>
</div>


<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="/js/main.js"></script>


</body>

</html>