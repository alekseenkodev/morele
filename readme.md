### Dzień dobry, rozwiązałem zadanie testowe
### Dla rozwiązania używalem Laravel Framework 5.6

## .zip w załącznikach

##Git repo
`https://bitbucket.org/alekseenkodev/morele.git`

##Instrukcje instalacji
* Web server(Apache/nginx)
* PHP >= 7.1.3
* Project directory /public
* composer install



##Jeśli wszystko jest w porządku
`http://joxi.ru/LmGD4ewueoLBq2`

##Zapisane zdjęcia będą w
`/public/images`
##Zapisane logs będa w 
`/storage/logs/laravel.log`

##Jeśli macie problemy z instalacją, poinformujcie mnie o tym e-mailem
Dziękuję za uwagę, życzę miłego dnia!