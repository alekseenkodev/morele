$('#upload_form').on('submit', function (e) {
    e.preventDefault();
    var file_data = $('#image').prop('files')[0];
    var width = $("#width").val();
    var height = $("#height").val();

    if (width < 0 || height < 0) {
        toastr.error('Enter correct width or height ', 'Error');
        e.preventDefault();
        return false;
    }

    var form_data = new FormData(this);


    $.ajax({
        url: '/upload',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (response) {
            if (response.status == 'ok') {
                toastr.success('All be ok!', 'Success');
                $("#image_box").append(
                    '<div><img src="/images/' + response.image + '" /></div>'
                )
            } else {
                toastr.error('Something went wrong ', 'Error');
                return false;
            }
        },
        error: function (response) {

            toastr.error('Something went wrong ', 'Error');
            return false;
        }
    });
});